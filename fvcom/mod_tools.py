# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 11:14:50 2016

functions used for FVCOM res 

@author: liuy
"""
import matplotlib.pyplot as plt
import numpy as np
import sys
import time
import matplotlib.tri as tri

def PointInsideTriangle2(pt,tri):
    '''checks if point pt(2) is inside triangle tri(3x2). @Developer'''
    a = 1/(-tri[1,1]*tri[2,0]+tri[0,1]*(-tri[1,0]+tri[2,0])+ \
        tri[0,0]*(tri[1,1]-tri[2,1])+tri[1,0]*tri[2,1])
    s = a*(tri[2,0]*tri[0,1]-tri[0,0]*tri[2,1]+(tri[2,1]-tri[0,1])*pt[0]+ \
        (tri[0,0]-tri[2,0])*pt[1])
    if s<0: return False
    else: t = a*(tri[0,0]*tri[1,1]-tri[1,0]*tri[0,1]+(tri[0,1]-tri[1,1])*pt[0]+ \
              (tri[1,0]-tri[0,0])*pt[1])
    return ((t>0) and (1-s-t>0))
    
def readgeo(geofile):
    f=open(geofile.encode('utf-8'),'r')
    file0=f.readlines()
    for eachline in file0:
        lines=eachline.split()
    nnode=int(lines[1])
    for eachline in file0:
        lines=eachline.split()
        if lines[0]=='GNN':
            ntri=int(lines0[1])
            break
        lines0=lines
    node=np.zeros((nnode,3),dtype=np.float64)
    tn=np.zeros((ntri,3),dtype=np.int)
    imt=np.zeros((ntri), dtype=np.int)
    ntri=0
    nnode=0
    for eachline in file0:
        lines=eachline.split()
        if lines[0]=='GE':
            tn[ntri,0]=int(lines[2])
            tn[ntri,1]=int(lines[4])
            tn[ntri,2]=int(lines[6])
            imt[ntri]=int(lines[-2])
            ntri+=1
        elif lines[0]=='GNN':
            node[nnode,0]=float(lines[2]) #xn
            node[nnode,1]=float(lines[3]) #yn
            node[nnode,2]=float(lines[4]) #zn
            nnode+=1
    return ntri,nnode,node,tn
def tri_area(tri):
    ''' tri [ 3*2 ] [:,0]:x [:,1]:y '''
    x1=tri[0,0];    x2=tri[1,0];    x3=tri[2,0]
    y1=tri[0,1];    y2=tri[1,1];    y3=tri[2,1]
    #area = 0.5*( x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2) )
    ll00=np.sqrt((x2-x1)**2+(y2-y1)**2) #length of the first len
    ll01=np.sqrt((x2-x3)**2+(y2-y3)**2)
    ll02=np.sqrt((x3-x1)**2+(y3-y1)**2)
    s=(ll00+ll01+ll02)*1./2.
    area=(s*(s-ll00)*(s-ll01)*(s-ll02))**0.5
    return area
    
def getLoc(elexy,ntri,node,nv):
    Loc=dict()
    tri00=np.zeros([3,2],dtype=float)
    tri01=np.zeros([3,2],dtype=float)
    tri02=np.zeros([3,2],dtype=float)
    for ii in range(0,elexy.shape[0]):
        xx=elexy[ii,0]
        yy=elexy[ii,1]
        for jj in range(0,ntri):
            # for ele cord in the center of tri
            # no_01
            tri00[0,0]=xx;    tri00[1,0]=node[nv[jj,0]-1,0]; tri00[2,0]=node[nv[jj,1]-1,0]
            tri00[0,1]=yy;    tri00[1,1]=node[nv[jj,0]-1,1]; tri00[2,1]=node[nv[jj,1]-1,1]
            # no_02
            tri01[0,0]=xx;    tri01[1,0]=node[nv[jj,1]-1,0]; tri01[2,0]=node[nv[jj,2]-1,0]
            tri01[0,1]=yy;    tri01[1,1]=node[nv[jj,1]-1,1]; tri01[2,1]=node[nv[jj,2]-1,1]
            # no_02
            tri02[0,0]=xx;    tri02[1,0]=node[nv[jj,2]-1,0]; tri02[2,0]=node[nv[jj,0]-1,0]
            tri02[0,1]=yy;    tri02[1,1]=node[nv[jj,2]-1,1]; tri02[2,1]=node[nv[jj,0]-1,1]
            tri00area=tri_area(tri00)
            tri01area=tri_area(tri01)
            tri02area=tri_area(tri02)
            if abs(tri00area+tri01area+tri02area-triarea[jj])<1.e-5:
                break
        Loc[ii]=jj
    return Loc

def getLoc00(elexy,ntri,node,nv):
    tri00=np.zeros([3,2],dtype=float)
    Loc=dict()
    for ii in range(0,elexy.shape[0]):
        for jj in range(0,ntri):
            tri00[0,0]=node[nv[jj,0]-1,0];    tri00[1,0]=node[nv[jj,1]-1,0]; tri00[2,0]=node[nv[jj,2]-1,0]
            tri00[0,1]=node[nv[jj,0]-1,1];    tri00[1,1]=node[nv[jj,1]-1,1]; tri00[2,1]=node[nv[jj,2]-1,1]
            if PointInsideTriangle2(elexy[ii,:],tri00):
                break
        Loc[ii]=jj
    return Loc 

def findNearestPoint(FX, FY, x, y, maxDistance=np.inf, noisy=False):
    """
    Given some point(s) x and y, find the nearest grid node in FX and
    FY.

    Returns the nearest coordinate(s), distance(s) from the point(s) and
    the index in the respective array(s).

    Optionally specify a maximum distance (in the same units as the
    input) to only return grid positions which are within that distance.
    This means if your point lies outside the grid, for example, you can
    use maxDistance to filter it out. Positions and indices which cannot
    be found within maxDistance are returned as NaN; distance is always
    returned, even if the maxDistance threshold has been exceeded.

    Parameters
    ----------
    FX, FY : ndarray
        Coordinates within which to search for the nearest point given
        in x and y.
    x, y : ndarray
        List of coordinates to find the closest value in FX and FY.
        Upper threshold of distance is given by maxDistance (see below).
    maxDistance : float, optional
        Unless given, there is no upper limit on the distance away from
        the source for which a result is deemed valid. Any other value
        specified here limits the upper threshold.
    noisy : bool or int, optional
        Set to True to enable verbose output. If int, outputs every nth
        iteration.

    Returns
    -------
    nearestX, nearestY : ndarray
        Coordinates from FX and FY which are within maxDistance (if
        given) and closest to the corresponding point in x and y.
    distance : ndarray
        Distance between each point in x and y and the closest value in
        FX and FY. Even if maxDistance is given (and exceeded), the
        distance is reported here.
    index : ndarray
        List of indices of FX and FY for the closest positions to those
        given in x, y.

    """

    if np.ndim(x) != np.ndim(y):
        raise Exception('Number of points in X and Y do not match')

    nearestX = np.empty(np.shape(x))
    nearestY = np.empty(np.shape(x))
    index = np.empty(np.shape(x))
    distance = np.empty(np.shape(x))

    # Make all values NaN
    nearestX = nearestX.ravel() * np.NaN
    nearestY = nearestY.ravel() * np.NaN
    index = index.ravel() * np.NaN
    distance = distance.ravel() * np.NaN

    if np.ndim(x) == 0:
        todo = np.column_stack([x, y])
        n = 1
    else:
        todo = list(zip(x, y))
        n = np.shape(x)[0]

    for c, pointXY in enumerate(todo):
        if type(noisy) == int:
            if c == 0 or (c + 1) % noisy == 0:
                print('Point {} of {}'.format(c + 1, n))
        elif noisy:
            print('Point {} of {}'.format(c + 1, n))

        findX, findY = FX - pointXY[0], FY - pointXY[1]
        vectorDistances = np.sqrt(findX**2 + findY**2)
        if vectorDistances.min() > maxDistance:
            distance[c] = np.min(vectorDistances)
            # Should be NaN already, but no harm in being thorough
            index[c], nearestX[c], nearestY[c] = np.NaN, np.NaN, np.NaN
        else:
            distance[c] = vectorDistances.min()
            index[c] = vectorDistances.argmin()
            nearestX[c] = FX[index[c]]
            nearestY[c] = FY[index[c]]

    # Convert the indices to ints if we don't have any NaNs.
    if not np.any(np.isnan(index)):
        index = index.astype(int)

    return nearestX, nearestY, distance, index



def unstructuredGridVolume(FVCOM):
    """
    Calculate the volume for every cell in the unstructured grid.

    Parameters
    ----------
    FVCOM : dict
        Dict which contains the following keys:
            - art1 - element area
            - h - water depth
            - zeta - surface elevation time series
            - siglev - sigma level layer thickness (range 0-1)

    Returns
    -------
    allVolumes : np.ndarray
        Array with the volumes of all the elements with time.

    """

    elemAreas = FVCOM['art1']
    elemDepths = FVCOM['h']
    elemTides = FVCOM['zeta']
    elemThickness = np.abs(np.diff(FVCOM['siglev'], axis=0))

    # Get volumes for each cell at each time step to include tidal changes
    tt, xx = FVCOM['zeta'].shape # time, node
    ll = FVCOM['siglev'].shape[0] - 1 # layers = levels - 1
    allVolumes = ((
        elemDepths + np.tile(elemTides, [ll, 1, 1]).transpose(1, 0, 2)
        ) * np.tile(elemThickness, [tt, 1, 1])) * elemAreas

    return allVolumes


def animateModelOutput(FVCOM, varPlot, startIdx, skipIdx, layerIdx, meshFile, addVectors=False, noisy=False):
    """
    Animated model output (for use in ipython).

    Give a NetCDF object as the first input (i.e. the output of readFVCOM()).
    Specify the variable of interest as a string (e.g. 'DYE'). This is case
    sensitive. Specify a starting index, a skip index of n to skip n time steps
    in the animation. The layerIdx is either the sigma layer to plot or, if
    negative, means the depth averaged value is calculated. Supply an
    unstructured grid file (FVCOM format).

    Optionally add current vectors to the plot with addVectors=True which will
    be colour coded by their magnitude.

    Noisy, if True, turns on printing of various bits of potentially
    relevant information to the console.

    """

    try:
        [triangles, nodes, x, y, z] = parseUnstructuredGridFVCOM(meshFile)
    except:
        print('Couldn''t import unstructured grid. Check specified file is the correct format')

    Z = FVCOM[varPlot]

    if layerIdx < 0:
        # Depth average the input data
        Z = dataAverage(Z, axis=1)

    plt.figure(200)
    plt.clf()

    # Initialise the plot
    plt.tripcolor(
        FVCOM['x'],
        FVCOM['y'],
        triangles,
        np.zeros(np.shape(FVCOM['x'])),
        shading='interp')
    plt.axes().set_aspect('equal', 'datalim')
    plt.colorbar()
    #plt.clim(-10, 10)
    plt.draw()

    # len(FVCOM['time'])+1 so range goes upto the length so that when i-1 is
    # called we get the last time step included in the animation.
    for i in range(startIdx, len(FVCOM['time'])+1, skipIdx):
        # Start animation at the beginning of the array or at startIdx-1
        # (i.e. i-2), whichever is larger.
        if i == startIdx:
            getIdx = np.max([startIdx-1, 0])
        else:
            getIdx = i-1

        if len(np.shape(Z)) == 3: # dim1=time, dim2=sigma, dim3=dye
            plotZ = np.squeeze(Z[getIdx,layerIdx,:])
        else: # dim1=time, dim2=dye (depth averaged)
            # Can't do difference here because we've depth averaged
            plotZ = np.squeeze(Z[getIdx,:])

        # Update the plot
        plt.clf()
        plt.tripcolor(FVCOM['x'], FVCOM['y'], triangles, plotZ, shading='interp')
        plt.colorbar()
        #plt.clim(-2, 2)
        # Add the vectors
        plt.hold('on')
        if addVectors:
            UU = np.squeeze(FVCOM['u'][i,layerIdx,:])
            VV = np.squeeze(FVCOM['v'][i,layerIdx,:])
            CC = np.sqrt(UU**2 + VV**2)
            Q = plt.quiver(FVCOM['xc'], FVCOM['yc'], UU, VV, CC, scale=10)
            plt.quiverkey(Q, 0.5, 0.92, 1, r'$1 ms^{-1}$', labelpos='W')
        plt.axes().set_aspect('equal', 'datalim')
        plt.draw()
        plt.show()

        # Some useful output
        if noisy:
            print('{} of {} (date {.2f})'.format(i, len(FVCOM['time']), FVCOM['time'][i-1]))
            print('Min: {} Max: {} Range: {} Standard deviation: {}'.format(plotZ.min(), plotZ.max(), plotZ.max()-plotZ.min(), plotZ.std()))
        else:
            print()


def nodes2elems(nodes, tri):
    """
    Calculate a element centre value based on the average value for the
    nodes from which it is formed. This necessarily involves an average,
    so the conversion from nodes2elems and elems2nodes is not
    necessarily reversible.

    Parameters
    ----------
    nodes : ndarray
        Array of unstructured grid node values to move to the element
        centres.
    tri : ndarray
        Array of shape (nelem, 3) comprising the list of connectivity
        for each element.

    Returns
    -------
    elems : ndarray
        Array of values at the grid nodes.

    """

    nvert = np.shape(tri)[0]

    if np.ndim(nodes) == 1:
        elems = nodes[tri].mean(axis=-1)
    elif np.ndim(nodes) == 2:
        elems = nodes[..., tri].mean(axis=-1)
    else:
        raise Exception('Too many dimensions (maximum of two)')

    return elems

def elems2nodes(elems, tri, nvert):
    """
    Calculate a nodal value based on the average value for the elements
    of which it a part. This necessarily involves an average, so the
    conversion from nodes2elems and elems2nodes is not necessarily
    reversible.

    Parameters
    ----------
    elems : ndarray
        Array of unstructured grid element values to move to the element
        nodes.
    tri : ndarray
        Array of shape (nelem, 3) comprising the list of connectivity
        for each element.
    nvert : int
        Number of nodes (vertices) in the unstructured grid.

    Returns
    -------
    nodes : ndarray
        Array of values at the grid nodes.

    """

    count = np.zeros(nvert, dtype=int)

    # Deal with 1D and 2D element arrays separately
    if np.ndim(elems) == 1:
        nodes = np.zeros(nvert)
        for i, indices in enumerate(tri):
            n0, n1, n2 = indices
            nodes[n0] = nodes[n0] + elems[i]
            nodes[n1] = nodes[n1] + elems[i]
            nodes[n2] = nodes[n2] + elems[i]
            count[n0] = count[n0] + 1
            count[n1] = count[n1] + 1
            count[n2] = count[n2] + 1

    elif np.ndim(elems) > 1:
        # Horrible hack alert to get the output array shape for multiple
        # dimensions.
        nodes = np.zeros((list(np.shape(elems)[:-1]) + [nvert]))
        for i, indices in enumerate(tri):
            n0, n1, n2 = indices
            nodes[..., n0] = nodes[..., n0] + elems[..., i]
            nodes[..., n1] = nodes[..., n1] + elems[..., i]
            nodes[..., n2] = nodes[..., n2] + elems[..., i]
            count[n0] = count[n0] + 1
            count[n1] = count[n1] + 1
            count[n2] = count[n2] + 1

    # Now calculate the average for each node based on the number of
    # elements of which it is a part.
    nodes = nodes / count

    return nodes