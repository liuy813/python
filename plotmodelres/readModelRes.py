# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 20:22:17 2016

@author: yong
"""
import xlrd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.font_manager as fntm
import numpy as np
from datetime import date
#import os
import time
import unicodedata

def open_excel(file= u'验证表.xlsx'):
    try:
        data = xlrd.open_workbook(file)
        return data
    except Exception,e:
        print str(e)

#根据名称获取Excel表格中的数据   参数:file：Excel文件路径     colnameindex：表头列名所在行的所以  ，by_name：Sheet1名称
def excel_table_byname(file= u'验证表.xlsx',colnameindex=0,by_name=u'实测潮位'):
    data = open_excel(file)
    table = data.sheet_by_name(by_name)
    #nrows = table.nrows #行数
    ncols = table.ncols #列数
    rownames =  table.row_values(colnameindex) #第一行数据 
    colnames =  table.col_values(colnameindex) #第一列数据 
    list0 =[]
    for colnum in range(1,ncols): #第一列第一行为字符
        col = table.col_values(colnum)
        if col:
            app = {}
            for i in range(1,len(colnames)):
                try:
                    aa=float(col[i])
                except:
                    break
                app[colnames[i]] = aa
            list0.append(app)
            time0=colnames[1:]
            res={};
            ii=1
            for aa in list0:
                dd=[]
                for jj in range(0,len(aa)):
                    cc=rownames[ii].encode("utf-8")
                    dd.append(aa[time0[jj]])
                    res[cc]=dd
                ii=ii+1
    return res,rownames,colnames    
if __name__=="__main__":
#def main():
   #tables = excel_table_byindex()
   #for row in tables:
   #    print row
   font = fntm.FontProperties(family='times new roman',style='normal', size=9, weight='bold', stretch='normal')
   fontlegend = fntm.FontProperties(family='SimHei',style='normal', size=9, weight='bold', stretch='normal')   
   resObs  ,rownamesObs,colnamesObs   = excel_table_byname(u'验证表.xlsx',0,u'实测潮位')
   resModel,rownamesModel,colnamesModel = excel_table_byname(u'验证表.xlsx',0,u'计算潮位')
   kk=1
   timedelta=date.toordinal(date(1900,1,1))-date.toordinal(date(1,1,1))
   for ii in range(1,len(rownamesObs)-1):
       fig, ax = plt.subplots(figsize=(10,5),dpi=200)
       fig.set_size_inches(10.66/2.54, 5.27/2.54)  # inch2cm 2.54
       #ax=plt.figure(kk,figsize=(10,5),dpi=200)
       cc=rownamesObs[ii].encode("utf-8")
       aa=colnamesObs[1:len(resObs[cc])+1]
       aa0=[jj+timedelta for jj in aa]
       bb=resObs[cc]
       #plt.plot(aa0,bb,'ro',markeredgecolor='r',markersize=2.4, \
       #        markerfacecolor='none',alpha=1,label=u'实测潮位')
       plt.scatter(aa0,bb,s=5,facecolors='none',edgecolors='r',lw=0.95,alpha=0.95,label=u'实测潮位')
       plt.hold
       ee=colnamesModel[1:len(resModel[cc])+1]
       ee0=[jj+timedelta for jj in ee]
       dd=resModel[cc]
       plt.plot(ee0,dd,'k-',linewidth=1.,label=u'计算潮位')
       #plt.xlim([np.min(aa0)+12,np.min(aa0)+13])
       plt.xlim([42395.3333+timedelta,42396.4167+timedelta])
       plt.ylim([-3,3])
       # xlabel and ylabel
       #plt.xlabel('Time')
       plt.xlabel(u'时间',fontproperties='SimHei',fontsize=9)
       plt.ylabel(u'潮位(m)',fontproperties='SimHei',fontsize=9)
       plt.title(rownamesObs[ii], fontproperties='SimHei',fontsize=9)
       legend = plt.legend(loc='lower center', shadow=True,\
                           bbox_to_anchor=(0.55, -0.3),ncol=2,\
                           prop = fontlegend,frameon=False).get_frame().set_alpha(0.7)
       ax.xaxis.set_label_coords(0.5, -0.)
       ax.xaxis.set_major_locator(mdates.HourLocator(range(0, 25, 6))) 
       ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%S"))
       #ax.xaxis.set_minor_locator(mdates.HourLocator(range(0, 25, 1)))
       # Hide the right and top spines       
       ax.spines['right'].set_visible(False)
       ax.spines['top'].set_visible(False)
       # Only show ticks on the left and bottom spines
       ax.yaxis.set_ticks_position('left')
       ax.xaxis.set_ticks_position('bottom')
       ax.spines['bottom'].set_position(('zero'))
       ax.spines['bottom'].set_linewidth(0.8)
       ax.spines['left'].set_linewidth(0.8)
       #ylabels = [item.get_text() for item in ax.get_yticklabels()]
       #ylabels = [('%.1f' % float(ylabel)) for ylabel in ylabels]
       #ax.set_yticklabels(ylabels)
       for i in ax.xaxis.get_majorticklabels():
           i.set_fontproperties(font)
       for i in ax.yaxis.get_majorticklabels():
           i.set_fontproperties(font)
       ax.tick_params(axis='both',reset=False,which='both',length=3,width=1,direction='in',pad=5)
       plt.subplots_adjust(bottom=0.18,top=0.89,left=0.1,right=.98)
       fig.savefig('res'+cc.decode('utf-8')+'.jpg', format='jpg', dpi=200)
       kk=kk+1
    #aa= main()
