# -*- coding: utf-8 -*-
"""
Created on Sun Jun 12 20:02:29 2016

@author: yong
"""

import xlrd
import matplotlib.pyplot as plt

filename=u'验证表.xlsx'
by_name=u'实测潮位'
colnameindex=0
data = xlrd.open_workbook(filename)
table = data.sheet_by_name(by_name)
nrows = table.nrows #行数
ncols = table.ncols #列数
rownames =  table.row_values(colnameindex) #第一行数据 
colnames =  table.col_values(colnameindex) #第一列数据 
list0 =[]
for colnum in range(1,ncols): #第一列第一行为字符
    col = table.col_values(colnum)
    if col:
        app = {}
        for i in range(1,len(colnames)):
            try:
                aa=float(col[i])
            except:
                break
            app[colnames[i]] = aa
        list0.append(app)
time0=colnames[1:]
bb={};
ii=1
for aa in list0:
    dd=[]
    for jj in range(0,len(aa)):
        cc=rownames[ii].encode("utf-8")
        dd.append(aa[time0[jj]])
        bb[cc]=dd
    ii=ii+1
kk=1
for ii in range(1,len(rownames)):
    plt.figure(kk)
    cc=rownames[ii].encode("utf-8")
    plt.plot(bb[cc])
    kk=kk+1