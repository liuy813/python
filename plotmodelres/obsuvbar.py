# -*- coding: utf-8 -*-
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm, colors
import matplotlib.collections as collections
from mpl_toolkits.mplot3d.art3d import Line3DCollection, line_collection_2d_to_3d
from pylab import *
import scipy.io as sio 
from datetime import datetime as dt   #For  get_date_tag
from datetime import *
import datetime 
import time 
from sklearn.metrics import mean_squared_error
import  math
import matplotlib.dates as mdates
from matplotlib.dates import AutoDateFormatter
import matplotlib.ticker as mticker
import sys

def get_date_tag(roms_time, ref=(2006, 01, 01), format="%d %m %Y at %H:%M:%S"):
    '''
    tag = get_date_tag(roms_time)

    return date tag for roms_time (in second since initialisation).
    default reference time is January 1st 2006. 
    '''
    ref = time.mktime(dt(ref[0], ref[1], ref[2]).timetuple())
    timestamp = ref + roms_time
    tag = dt.fromtimestamp(timestamp).strftime(format)

    return tag
def add_date_labels(self, min_date, max_date):
        """Add date labels to the graph.
        """
        self.axes.set_xlim(min_date, max_date)
        locator = mpl.dates.AutoDateLocator()
        locator.set_axis(self.axes.xaxis)
        formatter = mpl.dates.DateFormatter("%b %d %Y")
        self.axes.xaxis.set_major_locator(locator)
        self.axes.xaxis.set_major_formatter(formatter)
   
lat1=[24.5286,24.5022,24.4547];
lon1=[118.2131,118.3328,118.0644];

days    = mdates.DayLocator()   # every year
hours   = mdates.HourLocator()  # every month
daysFmt = mdates.DateFormatter('%Y/%m/%d %H')

#== wutong  dadeng-jinmen  xiagu===%%
I=1; # 1 for wutong;  2 for dadeng; 3 for gulangyu
tt=8;season='winter';
option='long'
option='short'
if I==1:
    location='wutong'
    matfile='wutong.mat'
    data=sio.loadmat(matfile)
    c1=[881,1002,1122,1244]; #model data
    c2=[1001,1122,1242,1343];

    d1=[1,122,243,364];  #observe data
    d2=[121,242,363,463];
elif I==2:
    location='dadeng'
    matfile='dadeng.mat'
    data=sio.loadmat(matfile)
    cc1=2;
    c1=[878+cc1,1221+cc1,1564+cc1,1907+cc1];
    c2=[1220+cc1,1563+cc1,1906+cc1,2195];

    d1=[1,344,687,1030];
    d2=[343,686,1029,1315];

else:
    location='guly'
    matfile='guly.mat'
    data=sio.loadmat(matfile)
    cc1=0
    c1=[878+cc1,1221+cc1,1564+cc1,1907+cc1];
    c2=[1220+cc1,1563+cc1,1906+cc1,2195];

    d1=[1,344,687,1030];
    d2=[343,686,1029,1369];


uubar=data['uubar']  #U direction
vvbar=data['vvbar']  #V direction
ubar=data['ubar']    # observe u
ang0=data['ang0']     #observe ang

for iii in range(0,4):
    if option[1]=='o':
        l=range(c1[0]-tt,c2[len(c2)-1]-tt)
    else:
        l=range(c1[iii]-tt,c2[iii]-tt)
    if I==1:
        ii=163;jj=229;
    elif I==2:
        ii = 188; jj = 226;
    elif  I==3:
    #ii=128;jj=197;
        ii=134;jj=193;
    ncname='roms_his_2010fesuv_25_0001.nc'  
    f=Dataset(ncname)
    uba1=f.variables['ubar'];vba1=f.variables['vbar'];angle1=f.variables['angle']
    oceantime=f.variables['ocean_time']
    lonr=f.variables['lon_rho']
    latr=f.variables['lat_rho']
    if iii==0: 
         print  location,season,"Grid posi",lonr[jj-1,ii-1],latr[jj-1,ii-1]
         print  location,season,"Obs  Posi",lon1[I],lat1[I]
    else:
        print iii
    
    uba=uba1[l,jj-1,ii-1];vba=vba1[l,jj-1,ii-1]
    angle=angle1[jj-1,ii-1];
 #   time0=oceantime[l]
    time1=oceantime[l]/24/3600+date(1900,1,1).toordinal()-date(2006,1,1).toordinal();
    time0=time1*24*3600
    timeb=get_date_tag(time0[0])
    timef=get_date_tag(time0[time0.size-1])
    
    
    Uout=uba*np.cos(angle)-vba*np.sin(angle);
    Vout=vba*np.cos(angle)+uba*np.sin(angle);

#    vec=math.sqrt(Uout**2+Vout**2);
    vec=np.sqrt(Uout**2+Vout**2);
    ang_m=(pi/2-np.arctan2(Vout,Uout))/pi*180;
    
    for inde in range(0,ang_m.size-1):
        if ang_m[inde]<0:
             ang_m[inde]=ang_m[inde]+360

        
#    ang_m(ang_m<0)=ang_m(ang_m<0)+360;

 #   date0=num2date(time1)
    if option[1]=='o':
        lob=range(d1[0],d2[len(d2)-1])
    else:
        lob=range(d1[iii],d2[iii])
    ubar_o=uubar[lob];
    vbar_o=vvbar[lob];
    vec_o=ubar[lob];
    ang_o=ang0[lob];
    
    day0=int(timeb[0:2]);
    day1=int(timef[0:2]);
    month0=int(timeb[4:5]);month1=int(timef[4:5]);
    year0=int(timeb[6:10]);year1=int(timef[6:10]);
    hour0=int(timeb[14:16]);hour1=int(timef[14:16]);
    min0=int(timeb[17:19]);min1=int(timef[17:19]);
    if min0==59 :
        min0=0;
        hour0=hour0+1;
    if min1==59 :
        min1=0;
        hour1=hour1+1;
    # 设置起始时间
    date1 = datetime.datetime(year0, month0, day0, hour0, 0)
    # 设置终止时间
    date2 = datetime.datetime(year1, month1, day1, hour1, 0)
    # 设置时间间隔(一分钟对应一个数据)
    
    delta = datetime.timedelta(hours=1)
    # 生成matplotlib的时间对象
    dateCounts = mpl.dates.drange(date1, date2, delta)
#    dateCounts = mpl.dates.drange(timeb, timef, delta)
    # 设置横坐标的刻度的文字大小
    mpl.rc('xtick', labelsize=12)
    # 设置纵坐标的刻度的文字大小
    mpl.rc('ytick', labelsize=12)
    plt.close()
    # 设置图片大小，宽，高，英寸
    fig=plt.figure(figsize=(20,12))   
    # 开启网格
#    plt.grid(True)
    ax1=plt.subplot(4,1,1)
    plt.plot_date(dateCounts, Uout, '-,', color="b", linewidth=1.3)
    plt.plot_date(dateCounts, ubar_o/100, '-,', color="r", linewidth=1.3)
    add_date_labels(ax1, date1, date2)
    ax1.autoscale_view()   
    plt.title('uubar in east directio '+location)
    ax1.grid(True)
    legend(['Model','Observe'])
    plt.legend(loc="best")
    plt.ylabel("m/s", fontsize=20)
    plt.ylim(-0.6,1.2)
    ax2=plt.subplot(4,1,2)
    plt.plot_date(dateCounts, Vout, '-,', color="b", linewidth=1.3)
    plt.plot_date(dateCounts, vbar_o/100, '-,', color="r", linewidth=1.3)
    add_date_labels(ax2, date1, date2)
    ax2.autoscale_view() 
    plt.title('vvbar in north directio '+location) 
    ax2.grid(True)
    plt.ylabel("m/s", fontsize=20)
    
    ax3=plt.subplot(4,1,3)
    plt.plot_date(dateCounts, vec, '-,', color="b", linewidth=1.3)
    plt.plot_date(dateCounts, vec_o/100, '-,', color="r", linewidth=1.3)
    add_date_labels(ax3, date1, date2) 
    ax3.autoscale_view()
    plt.title('uvbar in  '+location)
    ax3.grid(True)
    plt.ylabel("m/s", fontsize=20)
    
    ax4=plt.subplot(4,1,4)
    plt.plot_date(dateCounts, ang_m, '-,', color="b", linewidth=1.2)
    plt.plot_date(dateCounts, ang_o, '-,', color="r", linewidth=1.3)
    add_date_labels(ax4, date1, date2) 
    ax4.autoscale_view()
    plt.title('uvang in  '+location)    
    ax4.grid(True)
    plt.ylim(0,360)
    plt.ylabel("Degree", fontsize=20)
    
    plt.xlabel("Time", fontsize=20)           
    fig.tight_layout() # Or equivalently,  "plt.tight_layout()"  
#    ax.xaxis.set_major_locator(days)
#    ax.xaxis.set_major_formatter(daysFmt)
#    ax.xaxis.set_minor_locator(hours)
# put no more than 10 ticks on the date axis.  
#    ax.xaxis.set_major_locator(mticker.MaxNLocator(10))
# format the date in our own way.
#    ax.xaxis.set_major_formatter(mdates.AutoDateFormatter())

    # rotate the labels on both date axes
#for label in ax.xaxis.get_ticklabels():
#    label.set_rotation(30)
#for label in ax.xaxis.get_ticklabels():
#    label.set_rotation(30)


#    plt.subplot_date(dateCounts, Uout, '-,', color="r", linewidth=1.3)
#    plt.subplot_date(dateCounts, ubar_o/100, '-,', color="b", linewidth=1.3)
    plt.show()
    plt.savefig("case"+ncname[19:21] + location + season+ str(iii)+ ".png")
    if option[1]=='o':
        sys.exit(0)
        

